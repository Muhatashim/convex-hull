package org.virus.cvxhull.api.ui;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PaintListener {
    public void onRepaint(Graphics2D g);
}
