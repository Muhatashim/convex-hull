package org.virus.cvxhull.api.ui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class Painter extends JPanel {
    private final JFrame              parent;
    public final  List<PaintListener> painters;

    public Painter(JFrame parent) {
        this.parent = parent;
        painters = new ArrayList<>();
    }

    public void clear() {
        synchronized (Painter.class) {
            painters.clear();
        }
    }

    public boolean add(PaintListener paintListener) {
        synchronized (Painter.class) {
            return painters.add(paintListener);
        }
    }

    @Override
    public void paint(Graphics g) {
        synchronized (Painter.class) {
            for (PaintListener listener : painters)
                listener.onRepaint((Graphics2D) g.create());
            parent.repaint();
        }
    }
}
