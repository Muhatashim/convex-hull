package org.virus.cvxhull.api.algorithms;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 10:08 PM
 */
public class QuickHull extends ConvexHullAlgorithm {

    private List<Integer> hullPoints;

    protected QuickHull(int[] xs, int[] ys) {
        super(xs, ys);
        hullPoints = new ArrayList<>();
    }

    @Override
    public double bigO() {
        return len * Math.log(len);
    }

    private double dist(int from, int to) {
        return Math.abs(xs[to] - xs[from]) + Math.abs(ys[to] - ys[from]);
    }

    private List<Integer> getPointsOnRightSide(List<Integer> area, int a, int b) {
        List<Integer> side = new ArrayList<>();

        //Not really any other method to make this look any neater w/out changing lowering efficiency....
        if (area != null) {
            for (int i : area)
                if (helper.crossProduct(a, b, i) < 0)
                    side.add(i);
        } else
            for (int i = 0; i < len; i++)
                if (helper.crossProduct(a, b, i) < 0)
                    side.add(i);

        return side;
    }

    private int findFarthestPoint(List<Integer> pointsOnSide, int a, int b) {
        double maxDist = 0;
        int best = -1;

        if (pointsOnSide != null)
            for (int i : pointsOnSide) {
                double currDist = dist(a, i) + dist(b, i);

                if (currDist > maxDist) {
                    maxDist = currDist;
                    best = i;
                }
            }

        return best;
    }

    /**
     * @return [mostLeft, mostRight]
     */
    private int[] findExtremeXPoints() {
        int extremes[] = new int[2];

        for (int i = 1; i < len; i++) {
            if (xs[i] < xs[extremes[0]] || (xs[i] == xs[extremes[0]] && ys[i] < ys[extremes[0]]))
                extremes[0] = i;
            else if (xs[i] > xs[extremes[1]] || (xs[i] == xs[extremes[0]] && ys[i] > ys[extremes[0]]))
                extremes[1] = i;
        }
        return extremes;
    }

    @Override
    protected List<Point> generateHull() {
        int[] extremeXPoints = findExtremeXPoints();
        int mostLeft = extremeXPoints[0];
        int mostRight = extremeXPoints[1];

        hullPoints.add(mostLeft);
        findHull(getPointsOnRightSide(null, mostLeft, mostRight), mostLeft, mostRight);

        hullPoints.add(mostRight);
        findHull(getPointsOnRightSide(null, mostRight, mostLeft), mostRight, mostLeft);

        hullPoints.add(mostLeft);
        return helper.toPointList(hullPoints);
    }

    private void findHull(List<Integer> area, int left, int right) {
        if (area != null && area.isEmpty())
            return;

        int c = findFarthestPoint(area, left, right);

        findHull(getPointsOnRightSide(area, left, c), left, c);
        hullPoints.add(c);
        findHull(getPointsOnRightSide(area, c, right), c, right);
    }
}