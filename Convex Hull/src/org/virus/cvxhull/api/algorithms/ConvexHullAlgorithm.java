package org.virus.cvxhull.api.algorithms;


import org.virus.cvxhull.api.ui.PaintListener;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ConvexHullAlgorithm implements PaintListener, Comparable<ConvexHullAlgorithm> {
    protected final int[]       xs;
    protected final int[]       ys;
    protected final int         len;
    protected final Helper      helper;
    private         List<Point> hullPoints;

    /**
     * Precondition: xs.length == ys.length && xs.length >= 2
     *
     * @param xs all the x coordinates of the object
     * @param ys all the y coordinates of the object
     */
    protected ConvexHullAlgorithm(int[] xs, int[] ys) {
        this.xs = xs;
        this.ys = ys;
        len = xs.length;
        helper = new Helper(this);
    }

    public abstract double bigO();

    protected abstract List<Point> generateHull();

    /**
     * @return how long it took to calculate the hull in millisecs.
     */
    public final long run() {
        long startTime = System.currentTimeMillis();
        hullPoints = generateHull();
        return System.currentTimeMillis() - startTime;
    }

    public final Point[] getHullArray() {
        return hullPoints != null ?
                hullPoints.toArray(new Point[hullPoints.size()]) :
                null;
    }

    @Override
    public int compareTo(ConvexHullAlgorithm alg2) {
        return (int) Math.round(bigO() - alg2.bigO());
    }

    @Override
    public String toString() {
        return "ConvexHullAlgorithm{" +
                "len=" + len +
                ", hullPoints=" + hullPoints +
                '}';
    }

    @Override
    public final void onRepaint(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for (int i = 0; i < len; i++)
            g.fillOval(xs[i] - 1, ys[i] - 1, 2, 2);

        g.setColor(Color.red);

        Point[] hullArray = getHullArray();
        if (hullArray != null) {
            g.setColor(Color.magenta);
            for (int i = 1; i < hullArray.length; i++) {
                if (hullArray[i - 1] != null && hullArray[i] != null)
                    g.drawLine(hullArray[i - 1].x, hullArray[i - 1].y, hullArray[i].x, hullArray[i].y);
            }
        }
    }

    public static enum Algorithm {
        GIFT_WRAPPING(GiftWrapping.class),
        QUICK_HULL(QuickHull.class);

        private final Class<? extends ConvexHullAlgorithm> clazz;

        Algorithm(Class<? extends ConvexHullAlgorithm> clazz) {
            this.clazz = clazz;
        }

        public ConvexHullAlgorithm newInstance(int[] xs, int[] ys) {
            try {
                return clazz.getDeclaredConstructor(int[].class, int[].class).newInstance(xs, ys);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
