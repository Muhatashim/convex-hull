package org.virus.cvxhull.api.algorithms;

import org.virus.cvxhull.api.ui.PaintListener;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class GiftWrapping extends ConvexHullAlgorithm implements PaintListener {
    protected GiftWrapping(int[] xs, int[] ys) {
        super(xs, ys);
    }

    @Override
    public double bigO() {
        Point[] hullArray = getHullArray();
        return len * (hullArray != null ? hullArray.length : -1);
    }


    @Override
    protected List<Point> generateHull() {
        List<Point> hull = new ArrayList<>();

        int bottomLeftPoint = helper.findBottomLeftPoint();
        int curr = bottomLeftPoint;
        hull.add(helper.toPoint(curr));

        do {
            int leftMost = 1;

            for (int i = 0; i < len; i++) {
                if (i != leftMost && helper.crossProduct(curr, leftMost, i) > 0)
                    leftMost = i;
            }

            hull.add(helper.toPoint(leftMost));

            if (leftMost != 1)
                curr = leftMost;
            else
                curr = bottomLeftPoint;

        } while (curr != bottomLeftPoint);

        return hull;
    }
}
