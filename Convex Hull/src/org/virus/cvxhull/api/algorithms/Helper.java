package org.virus.cvxhull.api.algorithms;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 8:07 PM
 */
public class Helper {
    private final ConvexHullAlgorithm algo;

    public Helper(ConvexHullAlgorithm algo) {
        this.algo = algo;
    }

    public int findBottomLeftPoint() {
        int leastIndex = 0;

        for (int i = 0; i < algo.len; i++) {
            if (algo.xs[i] == algo.xs[leastIndex] && algo.ys[i] < algo.ys[leastIndex])
                leastIndex = i;

            if (algo.xs[i] < algo.xs[leastIndex]) {
                leastIndex = i;
            }
        }
        return leastIndex;
    }

    /**
     * @param a point1 of line
     * @param b point2 of line
     * @param c iteration
     * @return crossProduct > 0 if current iteration is left of the line from c to l
     *         or crossProduct < 0 if right or crossProduct == 0 is the three points are collinear.
     */
    public int crossProduct(int a, int b, int c) {
        int aX = algo.xs[a];
        int aY = algo.ys[a];
        int bX = algo.xs[b];
        int bY = algo.ys[b];
        int cX = algo.xs[c];
        int cY = algo.ys[c];

        return ((bX - aX) * (cY - aY) - (cX - aX) * (bY - aY));
    }

    public List<Point> toPointList(List<Integer> indexes) {
        List<Point> points = new ArrayList<>(indexes.size());

        for (int i = 0; i < indexes.size(); i++)
            points.add(toPoint(indexes.get(i)));

        return points;
    }

    public List<Point> toPointList() {
        List<Point> points = new ArrayList<>(algo.len);

        for (int i = 0; i < algo.len; i++)
            points.add(toPoint(i));

        return points;
    }

    public Point toPoint(int i) {
        return new Point(algo.xs[i], algo.ys[i]);
    }
}
