package org.virus.cvxhull.api;

import org.virus.cvxhull.api.algorithms.ConvexHullAlgorithm;
import org.virus.cvxhull.api.ui.Painter;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Tester {

    private final int                           xSpan;
    private final int                           ySpan;
    private final boolean                       putInCenter;
    private final int[]                         xs;
    private final int[]                         ys;
    private       ConvexHullAlgorithm.Algorithm algorithm;

    public Tester(ConvexHullAlgorithm.Algorithm algorithm,
                  int size, int xSpan, int ySpan, boolean putInCenter) {
        this.algorithm = algorithm;
        this.xSpan = xSpan;
        this.ySpan = ySpan;
        this.putInCenter = putInCenter;

        xs = new int[size];
        ys = new int[size];
        randomizeGrid();
    }

    public void setAlgorithm(ConvexHullAlgorithm.Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public void test(Painter painter) {
        ConvexHullAlgorithm algo = algorithm.newInstance(xs, ys);

        painter.add(algo);
        algo.run();
        painter.repaint();
    }

    public void testLog(Painter painter) {
        ConvexHullAlgorithm algo;

        System.out.println("Testing " + algorithm);

        System.out.println("\tInitializing algorithm....");
        algo = algorithm.newInstance(xs, ys);
        painter.add(algo);
        painter.repaint();

        System.out.printf("\tRunning algorithm with %d elements with an x span of %d and y span of %d\n",
                xs.length, xSpan, ySpan);
        long runTime = algo.run();
        System.out.printf("\tFinished test, took %dms to calculate | O = %f\n" +
                "\tPoints on hull: %d\n",
                runTime, algo.bigO(), algo.getHullArray().length);
    }

    private void randomizeGrid() {
        Random r = new Random();
        int halfXSpan;
        int halfYSpan;

        if (putInCenter) {
            halfXSpan = xSpan / 2;
            halfYSpan = ySpan / 2;
        } else {
            halfXSpan = 0;
            halfYSpan = 0;
        }

        for (int i = 0; i < xs.length; i++) {
            xs[i] = r.nextInt(xSpan) - halfXSpan;
            ys[i] = r.nextInt(ySpan) - halfYSpan;
        }
    }
}
