package org.virus.cvxhull;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainFrame extends JFrame {
    private org.virus.cvxhull.api.ui.Painter painter;

    public MainFrame() {
        add(painter = new org.virus.cvxhull.api.ui.Painter(this));

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);
    }

    public org.virus.cvxhull.api.ui.Painter getPainter() {
        return painter;
    }
}
