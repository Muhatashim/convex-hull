package org.virus.cvxhull;

import org.virus.cvxhull.api.Tester;
import org.virus.cvxhull.api.algorithms.ConvexHullAlgorithm;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    private static MainFrame mainFrame;

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                mainFrame = new MainFrame();
            }
        });

        for (int i = 0; i < 10000; i++) {
            Tester tester = new Tester(ConvexHullAlgorithm.Algorithm.GIFT_WRAPPING,
                    1000, 1000, 1000, false);
            mainFrame.getPainter().clear();
            tester.test(mainFrame.getPainter());
        }
    }
}
